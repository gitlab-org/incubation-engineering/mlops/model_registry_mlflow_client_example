# MLflow Model Registry client example

An example of using MLflow client API to create a model version.

## Setup

1. Install requirements
    ```
    pip install -R requirements.txt
    ```

2. Launch Jupyter lab with
    ```
    jupyter lab
    ```

## Run the MLflow server

```
mlflow server --backend-store-uri sqlite:///mlruns.db
```
